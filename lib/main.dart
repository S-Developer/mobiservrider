import 'package:flutter/material.dart';
import 'package:mobiservrider/Account/LoginPage.dart';
import 'package:path/path.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'Account/authentication.dart';
import 'Account/root_page.dart';
import 'HomePage.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget { 
  initState(BuildContext context)
  {
    checkLogin(context);
  }

  void checkLogin(BuildContext context) async{
    SharedPreferences preferences = await SharedPreferences.getInstance();
    String userID = preferences.getString("UserID");
    try{
      if(userID ==""){
         Navigator.of(context).pop();  
                          
                          Navigator.push(context, MaterialPageRoute(builder: (context)=>LoginSignUpPage()));
        
      }
      else{
         Navigator.of(context).pop();  
                          
                          Navigator.push(context, MaterialPageRoute(builder: (context)=>HomePage()));
      }

    }catch(e){
      Navigator.of(context).pop();  
                          
                          Navigator.push(context, MaterialPageRoute(builder: (context)=>HomePage()));
    }

  }
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
      // primarySwatch: Colors.blueAccent,
      ),
       home: new RootPage(auth: new Auth()));
    
  }
}
