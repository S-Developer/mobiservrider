import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import '../HomePage.dart';
import 'authentication.dart';
import 'package:http/http.dart' as http;

class AccountReg extends StatefulWidget {
  AccountReg({this.auth, this.onSignedIn});

  final BaseAuth auth;
  final VoidCallback onSignedIn;

  @override
  State<StatefulWidget> createState() => new _AccountRegState();
}

enum FormMode { LOGIN, SIGNUP }

class _AccountRegState extends State<AccountReg> {
  final _formKey = new GlobalKey<FormState>();
  static final String uploadEndPoint =
      'http://nortonconstituencyzim.co.zw/json/driverRegistration.php';
   

  String _email;
  String _password;
  String _errorMessage;
  String _firstname;
  String _lastname;
  String _phone;
  String _address;


  // Initial form is login form
  FormMode _formMode = FormMode.LOGIN;
  bool _isIos;
  bool _isLoading;
  String _userID;




   getCurrentUser() async{
    final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
      FirebaseUser user = await _firebaseAuth.currentUser();
      setState(() {
       _email=user.email;
       _userID = user.uid;
       print("++++++++++++++++++++++++++++");
       print(_userID);

       
      });
       
     
  }

  // Check if form is valid before perform login or signup
  bool _validateAndSave() {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  // Perform login or signup
  void _validateAndSubmit() async {
    setState(() {
      _errorMessage = "";
      _isLoading = true;
    });
    if (_validateAndSave()) {
      String userId = ""; 
          DateTime now = new DateTime.now();
          var datestamp = new DateFormat("yyyy-MM-dd");
          String currentdate = datestamp.format(now); 
      try {
        http.post(uploadEndPoint, body:{
           "username": _userID,
      "firstname" : _firstname,
      "lastname": _lastname,
      "email":_email,
      "password": "_password",
      "phone":_phone,
      "address":_address,
      "level":"7",
      "status":"Pending",
      "joindate": currentdate,
      "departmentid": "7"
        }).then((result){
          if(result != "false"){
            FirebaseDatabase.instance.reference().child('Profile').child(_userID)
        .set({'userID': _userID,'Fullname': _firstname+_lastname, 'Email':_email, 'created_at':currentdate});
                          showDialog(
                          context: context,
                          builder: (BuildContext context) {
                          return AlertDialog(

                          content: new Text("Account Registered successfully"),

                          actions: <Widget>[
                          new FlatButton(
                          child: new Text("Close"),
                          onPressed: () {
                          Navigator.pop(context);
                          Navigator.pop(context);
                          Navigator.push(context, MaterialPageRoute(builder: (context)=>HomePage()));
                          },
                          ),

                          ],
                          );
                          },);

        
        Navigator.push(context, MaterialPageRoute(builder: (context)=> HomePage()));

          }
        });
      
       
     
        setState(() {
          _isLoading = false;
        });

        if (userId != null && userId.length > 0 && _formMode == FormMode.LOGIN) {
          widget.onSignedIn();
        }

      } catch (e) {
        print('Error: $e');
        setState(() {
          _isLoading = false;
          if (_isIos) {
            _errorMessage = e.details;
          } else
            _errorMessage = e.message;
        });
      }
    }
  }


  @override
  void initState() {
    _errorMessage = "";
    _isLoading = false;
     getCurrentUser();
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    _isIos = Theme.of(context).platform == TargetPlatform.iOS;
    return new Scaffold(
        
        body: Stack(
          children: <Widget>[
            _showBody(),
            _showCircularProgress(),
          ],
        ));
  }

  Widget _showCircularProgress(){
    if (_isLoading) {
      return Center(child: CircularProgressIndicator());
    } return Container(height: 0.0, width: 0.0,);

  }


  Widget _showBody(){
    return new Container(
        padding: EdgeInsets.all(16.0),
        child: new Form(
          key: _formKey,
          child: new ListView(
            shrinkWrap: true,
            children: <Widget>[
              _showLogo(),
              _showFirstname(),
              _showLastname(),
              _showPhone(),
              _showAddress(),
              _showCircularProgress(),
              _showPrimaryButton(),
              _showErrorMessage(),
            ],
          ),
        ));
  }

  Widget _showErrorMessage() {
    if (_errorMessage != null && _errorMessage.length > 0) {
      return new Text(
        _errorMessage,
        style: TextStyle(
            fontSize: 13.0,
            color: Colors.red,
            height: 1.0,
            fontWeight: FontWeight.w300),
      );
    } else {
      return new Container(
        height: 0.0,
      );
    }
  }

  Widget _showLogo() {
    return new Hero(
      tag: 'hero',
      child: FlutterLogo(size: 100.0),
    );
  }

   Widget _showFirstname() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 15, 0.0, 0.0),
      child: new TextFormField(
        maxLines: 1,
        keyboardType: TextInputType.text,
        autofocus: false,
        decoration: new InputDecoration(
            hintText: 'Firstname',
            icon: new Icon(
              Icons.person_pin,
              color: Colors.grey,
            )),
        validator: (value) {
          if (value.isEmpty) {
            setState(() {
              _isLoading = false;
            });
            return 'Email can\'t be empty';
          }
        },
        onSaved: (value) => _firstname = value,
      ),
    );
  }
   Widget _showLastname() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 15, 0.0, 0.0),
      child: new TextFormField(
        maxLines: 1,
        keyboardType: TextInputType.text,
        autofocus: false,
        decoration: new InputDecoration(
            hintText: 'Lastname',
            icon: new Icon(
              Icons.person_pin_circle,
              color: Colors.grey,
            )),
        validator: (value) {
          if (value.isEmpty) {
            setState(() {
              _isLoading = false;
            });
            return ' can\'t be empty';
          }
        },
        onSaved: (value) => _lastname = value,
      ),
    );
  }
   Widget _showPhone() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 15, 0.0, 0.0),
      child: new TextFormField(
        maxLines: 1,
        keyboardType: TextInputType.phone,
        autofocus: false,
        decoration: new InputDecoration(
            hintText: 'Phone Number',
            icon: new Icon(
              Icons.call,
              color: Colors.grey,
            )),
        validator: (value) {
          if (value.isEmpty) {
            setState(() {
              _isLoading = false;
            });
            return 'Email can\'t be empty';
          }
        },
        onSaved: (value) => _phone = value,
      ),
    );
  }
   Widget _showAddress() {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0.0, 15, 0.0, 0.0),
      child: new TextFormField(
        maxLines: 1,
        keyboardType: TextInputType.text,
        autofocus: false,
        decoration: new InputDecoration(
            hintText: 'Address',
            icon: new Icon(
              Icons.location_city,
              color: Colors.grey,
            )),
        validator: (value) {
          if (value.isEmpty) {
            setState(() {
              _isLoading = false;
            });
            return 'Address can\'t be empty';
          }
        },
        onSaved: (value) => _address = value,
      ),
    );
  }



  Widget _showPrimaryButton() {
    return new Padding(
        padding: EdgeInsets.fromLTRB(0.0, 45.0, 0.0, 0.0),
        child: SizedBox(
          height: 40.0,
          child: new RaisedButton(
            elevation: 5.0,
            shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(30.0)),
            color: Colors.blue,
            child: new Text("Create Profile",style: TextStyle(fontSize: 20.0,color: Colors.white)),
            onPressed: _validateAndSubmit
          ),
        ));
  }
}
