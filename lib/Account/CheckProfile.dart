import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/material.dart' as prefix0;
import 'package:intl/intl.dart';
import 'package:mobiservrider/Account/AccountReg.dart';
import 'package:mobiservrider/Account/Pending.dart';
import 'package:mobiservrider/HomePage.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

import 'authentication.dart';
class CheckProfile extends StatefulWidget {
    CheckProfile({Key key, this.auth, this.userId, this.onSignedOut})
      : super(key: key);
        final BaseAuth auth;
        String userId;
  final VoidCallback onSignedOut;
  @override
  _CheckProfileState createState() => _CheckProfileState();
}

class _CheckProfileState extends State<CheckProfile> {
     static final String uploadEndPoint =
      'http://nortonconstituencyzim.co.zw/json/driverRegistration.php';
      static final String getDriverIEndPoint =
      'http://nortonconstituencyzim.co.zw/json/getDriverID.php';
  String _email;
  String _password;
  String _errorMessage;
  String _firstname;
  String _lastname;
  String _phone;
  String _address;
  initState(){
    getCurrentUser();
  
  }
  getCurrentUser() async{
    final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
      FirebaseUser user = await _firebaseAuth.currentUser();
      setState(() {
       _email=user.email; 
       
      });
        _getID();
     
  }

  _getID() async{
  try{
   http.post(getDriverIEndPoint,body:{
     "email": _email,
 }).then((result){

print("++++++++++++++++++++++++++++");
print(result.body);



   if(result.body=="NA"){
     Navigator.pop(context);
     Navigator.push(context, MaterialPageRoute(builder: (context)=>HomePage()));
   }
   if(result.body =="true"){
         addID() async{
        SharedPreferences prefs = await SharedPreferences.getInstance();
        prefs.setString("UserID", result.body);
        
      }
      Navigator.pop(context);
      Navigator.push(context, MaterialPageRoute(builder: (context)=>HomePage()));
   }

     if(result.body =="false"){
       
      Navigator.pop(context);
      Navigator.push(context, MaterialPageRoute(builder: (context)=>Pending()));
   }


  
  
 });
  }catch(e){
    print(e);
  }
 }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(child: Text("Account Configuring"),),

      
    );
  }
}