import 'package:firebase_database/firebase_database.dart';

class Messages {

String _id;
  String _clientID;
  String _recipentID;
  String _userEmail;
  String _message;

  Messages(this._id,this._clientID,this._recipentID,this._userEmail, this._message);
  String get id => clientID;
  String get clientID => _clientID;
  String get recipentID => _recipentID;
  String get email => _userEmail;
  String get message => _message;
 
  Messages.fromSnapshot(DataSnapshot snapshot) {
    _id = snapshot.key;
    _clientID = snapshot.value['from'];
    _recipentID = snapshot.value['from'];
    _message = snapshot.value['text'];

}
}