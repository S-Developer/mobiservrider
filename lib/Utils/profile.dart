import 'package:firebase_database/firebase_database.dart';

class Profile {
  String _id;
  String _fullname;
  String _email;
 
  Profile(this._id, this._fullname, this._email);
 
 
  String get id => _id;
  String get fullname => _fullname;
  String get email => _email;
 
  Profile.fromSnapshot(DataSnapshot snapshot) {
    _id = snapshot.key;
    _fullname = snapshot.value['Fullname'];
    _email = snapshot.value['Email'];

    
  }
}