import 'dart:convert';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:mobiservrider/Chat/ChartPage.dart';

import 'Account/authentication.dart';
import 'TaskDetails.dart';


class HomePage extends StatefulWidget {  

   HomePage({Key key, this.auth, this.userId, this.onSignedOut})
      : super(key: key);

  final BaseAuth auth;
  final VoidCallback onSignedOut;
  final String userId;
    @override
    _HomePageState createState() => _HomePageState();
  }

 
  
  class Todo {
  final String title;
  final String details;
  final String deliverylocation;
  final String deliverylong;
  final String deliverylat;
  final String expectedDeliveryDate;
  final String assignedtime;
  final String userid;
  final String taskStatus;
  final String orderType;
  final String productID;
  final String serviceID;
  final String firstname;
  final String lastname;
  final String phone;
  final String email;
  final String taskid;




  Todo(this.title, this.details,this.deliverylocation,this.deliverylong, this.deliverylat,this.expectedDeliveryDate,
  this.assignedtime, this.userid, this.taskStatus,this.orderType,this.productID,this.serviceID,this.firstname,this.lastname,
  this.phone,this.email,this.taskid);
}

class _HomePageState extends State<HomePage> {
  String _email;
  List tasksdata;
  Future<String> getData() async{
    var response = await http.post(
      Uri.encodeFull('http://nortonconstituencyzim.co.zw/json/getTasks.php'),
      body: {
           "email": _email,
      },
      headers:{
        "Accept": "application/json",
     
      }
    );
    print(response.body);
    this.setState((){
     tasksdata = json.decode(response.body);

     
    });
    return "Success!";
  }
  @override
  void initState(){
    super.initState();
    this.getCurrentUser();
   // this.getData();
  }
    _signOut() async {
    try {
      await widget.auth.signOut();
      widget.onSignedOut();
    } catch (e) {
      print(e);
    }
  }


  getCurrentUser() async{
    final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
      FirebaseUser user = await _firebaseAuth.currentUser();
      setState(() {
       _email=user.email; 

       getData();
      });
     
  }
  @override
  Widget build(BuildContext context) {
 
    return Scaffold(
      appBar: AppBar(
        title: Text("Mobiserv Rider"),
        actions: <Widget>[
          new FlatButton(
                child: new Text('Logout',
                    style: new TextStyle(fontSize: 17.0, color: Colors.white)),
                onPressed: _signOut)
        ],),
        drawer: Drawer(),
         floatingActionButton: FloatingActionButton(
           child: Icon(Icons.chat_bubble),
           
        onPressed: () {
          Navigator.pop(context);
          Navigator.push(context, MaterialPageRoute(builder: (context)=>ChatPage()));
        },
        
      ),
      
      body: new ListView.builder(
        padding: EdgeInsets.all(5),
        
        itemCount: tasksdata ==null?0 :tasksdata.length,
        itemBuilder: (BuildContext context,int index){
          return new GestureDetector(
            onTap: (){
              Navigator.push(context,MaterialPageRoute(builder: (context) {
                var taskDetails = TaskDetails(todo: new Todo(tasksdata[index]['title'],
                tasksdata[index]['details'],tasksdata[index]['deliverylocation'],tasksdata[index]['deliverylong'],
                tasksdata[index]['deliverylat'],tasksdata[index]['expectedDeliveryDate'],tasksdata[index]['assignedtime'],tasksdata[index]['userid'],tasksdata[index]['taskStatus'],
                tasksdata[index]['OrderType'],tasksdata[index]['productID'],tasksdata[index]['serviceID'],tasksdata[index]['firstname'],tasksdata[index]['lastname'],
                tasksdata[index]['phone'],tasksdata[index]['email'],tasksdata[index]['taskid']));
                return taskDetails;
              }));

            },
              
                child: Card(
              
                  elevation: 2,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5.0),
                  ),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                       ListTile(
                        leading: Icon(Icons.flag),
                        title:  Text((tasksdata[index]['title']),style: TextStyle(fontSize: 16,fontWeight: FontWeight.bold),),
                        subtitle:  Column(
                          
                            children: <Widget>[
                              Align(
                                alignment: Alignment.centerLeft,
                                child:Text(
                          (tasksdata[index]['details']),style: TextStyle(fontSize: 14,fontWeight: FontWeight.normal),),
                              ),
                          Align(
                            alignment: Alignment.centerLeft,
                             child:Text("Assigned on "+ tasksdata[index]['assignedtime'],style: TextStyle(fontSize: 12,fontWeight: FontWeight.bold,color: Colors.blueAccent),)
                           
                          )],
                          ),
                        ),
                        
                        
                      
                    
                    ],
                  ),
                
              ),
                        
                      );
                    },
                    
                  ),
                  
                );
              }
            
             
}




// 

