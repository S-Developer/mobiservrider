import 'package:flutter/material.dart';
import 'package:flutter_rounded_progress_bar/flutter_rounded_progress_bar.dart';
import 'package:flutter_rounded_progress_bar/rounded_progress_bar_style.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:mobiservrider/Services.dart';

import 'package:http/http.dart' as http;
import 'HomePage.dart';
import 'Maps.dart';
import 'cart.dart';
                                class TaskDetails extends StatefulWidget {
                                Todo todo;


                                TaskDetails({Key key, this.todo}) : super(key: key);
                                @override
                                _TaskDetailsState createState() => _TaskDetailsState();
                                }

                                class _TaskDetailsState extends State<TaskDetails> {
                                double _percentage;
                                double _lat;
                                double _long;
                                 static final String updateEndPoint =
                                'http://nortonconstituencyzim.co.zw/json/updateTask.php';

                                @override
                                void initState() {
                                // TODO: implement initState
                                super.initState();
                                setState(() {
                                _percentage = double.parse(widget.todo.taskStatus);
                                _lat = double.parse(widget.todo.deliverylat);
                                _long =double.parse(widget.todo.deliverylong);
                                });
                                }

                                updateProgress()async{
                                  print(_percentage);
                                  http.post(updateEndPoint,body:{
                                    
                                     "taskid":widget.todo.taskid,
                                     "value" : "$_percentage",
                                  }).then((result) {
                                

                                  });
     

                                }

                                viewService(){
                                  String serviceType = widget.todo.orderType;
                                  if(serviceType =="BuyNow"){
                                  Navigator.push(context,MaterialPageRoute(builder: (context) {
                                  var serviceDetails = Services(servi: new ServiceID(widget.todo.serviceID,widget.todo.title));
                                  return serviceDetails;
                                  }));
                                  }

                                  
                                   if(serviceType =="Cart"){
                                  Navigator.push(context,MaterialPageRoute(builder: (context) {
                                  var serviceDetails = CustomerCart(servi: new ServiceID(widget.todo.serviceID,widget.todo.title));
                                  return serviceDetails;
                                  }));
                                 
                                      
                                    
                                  }
                                   if(serviceType =="Service"){
                                         Navigator.push(context,MaterialPageRoute(builder: (context) {
                var serviceDetails = Services(servi: new ServiceID(widget.todo.serviceID,widget.todo.title));
                return serviceDetails;
                                    }));
                                    
                                  }
                                   if(serviceType ==""){
                                      print("This is empty");
                                    
                                  }
                                }

                                
                                checkForCoodinates()async{
                                  print(_lat);
                                  if(_long == 900000.0){
                                    showDialog(
                                    context: context,
                                    builder: (BuildContext context) {
                                    return AlertDialog(

                                    content: new Text("Map location not provided"),

                                    actions: <Widget>[
                                    new FlatButton(
                                    child: new Text("Close"),
                                    onPressed: () {
                                    Navigator.of(context).pop();  

                                    },
                                    ),

                                    ],
                                    );
                                    },);
                                    
                                  }
                                  else{
                                  Navigator.push(context,MaterialPageRoute(builder: (context) {
                                  var taskDetails = MyLocation(coodinates: new Coodinates(_lat,_long));
                                  return taskDetails;
                                  }));
                                  }
                                  
                                // Navigator.push(context, MaterialPageRoute(builder: (context)=>MyLocation()));

                                }
                                @override
                                Widget build(BuildContext context) {
                                return Scaffold(
                                appBar: AppBar(
                                title: Text(widget.todo.title),
                                ),
                                body:
                                Column(

                                children :<Widget>[

                                Column(
                                ),
                                Expanded(
                                child: new ListView(
                                children: <Widget>[
                                Padding(padding: EdgeInsets.all(12),
                                ),
                                Container(
                                color: Colors.white,
                                padding: EdgeInsets.fromLTRB(12, 12, 12, 0),
                                child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                Expanded(
                                child: Column(

                                children: <Widget>[
                                Padding(
                                padding: const EdgeInsets.only(bottom:1.0),
                                child: Text(widget.todo.details, style: TextStyle(fontSize: 25.0, fontWeight: FontWeight.bold),),
                                ),
                                ],
                                crossAxisAlignment: CrossAxisAlignment.start,
                                ),
                                flex: 9,
                                ),

                                ],
                                ),
                                ),
                                Divider(),
                                Padding(
                                padding: const EdgeInsets.only(left: 12),
                                child: Text("Task Completed")
                                ),

                                Padding(
                                padding: const EdgeInsets.all(0.0),
                                child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[

                                Expanded(
                                child: Column(
                                children: <Widget>[

                                RoundedProgressBar(

                                style: RoundedProgressBarStyle(
                                borderWidth: 0, 
                                widthShadow: 0),
                                margin: EdgeInsets.symmetric(vertical: 5),
                                borderRadius: BorderRadius.circular(10),
                                milliseconds:2000,
                                childLeft: Text("$_percentage"+"%  Completed",
                                style: TextStyle(color: Colors.white)),
                                percent: _percentage,
                                theme: RoundedProgressBarTheme.blue)

                                ],
                                crossAxisAlignment: CrossAxisAlignment.start,
                                ),

                                ),



                                ],
                                ),
                                ),


                                Padding(
                                padding: const EdgeInsets.all(12.0),
                                child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                Expanded(
                                child: Column(
                                children: <Widget>[
                                Padding(
                                padding: const EdgeInsets.only(bottom:0),
                                child: 
                                FlatButton.icon(
                                color: Colors.green,
                                onPressed: (){
                                setState(() {
                                _percentage = _percentage +25; 
                                updateProgress();
                                });
                                },
                                icon: Icon(Icons.add,color: Colors.white,), label: Text("Update Task",style: TextStyle(color: Colors.white),)
                                ),
                                ),

                                ],
                                crossAxisAlignment: CrossAxisAlignment.start,
                                ),

                                ),

                                Expanded(
                                child: Column(
                                children: <Widget>[
                                Padding(
                                padding: const EdgeInsets.all(0),
                                child: 
                                FlatButton.icon(
                                color: Colors.red,
                                onPressed: (){
                                setState(() {
                                _percentage = _percentage -25; 
                                updateProgress();
                                });
                                },
                                icon: Icon(Icons.exposure_neg_1,color: Colors.white,), label: Text("Update Task",style: TextStyle(color: Colors.white),)
                                ),
                                ),
                                //   Text('Avion Learning', style: TextStyle(color: Colors.black54),)
                                ],
                                crossAxisAlignment: CrossAxisAlignment.start,
                                ),

                                ),

                                ],
                                ),
                                ),
                                Divider(),
                                Padding(
                                padding: EdgeInsets.fromLTRB(12, 12, 12, 0),
                                // padding: const EdgeInsets.all(12.0),
                                child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                Expanded(
                                child: Column(
                                children: <Widget>[
                                Padding(
                                padding: const EdgeInsets.fromLTRB(0, 0, 0, 10),
                                child: Text('Delivery Address'),
                                ),
                                Padding(
                                padding: const EdgeInsets.only(bottom:1.0),
                                child: Text(widget.todo.deliverylocation, style: TextStyle(fontSize: 16.0, color: Colors.blueAccent),),
                                ),
                                ],
                                crossAxisAlignment: CrossAxisAlignment.start,
                                ),
                                flex: 9,
                                ),
                                Expanded(
                                child: Icon(FontAwesomeIcons.addressCard),
                                )

                                ],
                                ),
                                ),
                                Padding(
                                padding: EdgeInsets.fromLTRB(12, 12, 12, 0),
                                // padding: const EdgeInsets.all(12.0),
                                child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                Expanded(
                                child: Column(
                                children: <Widget>[
                                Padding(
                                padding: const EdgeInsets.fromLTRB(0, 0, 0, 10),
                                child: Text('Delivery expected at'),
                                ),
                                Padding(
                                padding: const EdgeInsets.only(bottom:1.0),
                                child: Text(widget.todo.expectedDeliveryDate, style: TextStyle(fontSize: 16.0, color: Colors.blueAccent),),
                                ),
                                ],
                                crossAxisAlignment: CrossAxisAlignment.start,
                                ),
                                flex: 9,
                                ),
                                Expanded(
                                child: Icon(FontAwesomeIcons.calendar),
                                )

                                ],
                                ),
                                ),
                                Divider(),
                                Padding(
                                padding: EdgeInsets.fromLTRB(12, 12, 12, 0),
                                // padding: const EdgeInsets.all(12.0),
                                child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                Expanded(
                                child: Column(
                                children: <Widget>[
                                Padding(
                                padding: const EdgeInsets.fromLTRB(0, 0, 0, 10),
                                child: Text('Order type'),
                                ),
                                Padding(
                                padding: const EdgeInsets.only(bottom:1.0),
                                child: Text(widget.todo.orderType, style: TextStyle(fontSize: 16.0, color: Colors.blueAccent),),
                                ),
                                ],
                                crossAxisAlignment: CrossAxisAlignment.start,
                                ),
                                flex: 9,
                                ),
                                Expanded(
                                child: Icon(FontAwesomeIcons.addressCard),
                                )

                                ],
                                ),
                                ),
                                Divider(),
                                  Padding(
                                    
                                padding: EdgeInsets.fromLTRB(12, 12, 12, 0),
                                // padding: const EdgeInsets.all(12.0),
                                child: Card(
                                  elevation: 10,
                                  child:Container(
                                    padding: EdgeInsets.all(10),
                                  color: Colors.lightBlueAccent,
                                  child: Row(
                                  
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                Expanded(
                                child: Column(
                                children: <Widget>[
                                Padding(
                                padding: const EdgeInsets.fromLTRB(0, 0, 0, 10),
                                child: Text('Customer Details',
                                style: TextStyle(fontWeight: FontWeight.normal,
                                color: Colors.white, fontSize: 18),),
                                ),
                                Divider(),
                                Padding(
                                padding: const EdgeInsets.only(bottom:1.0),
                                child: Text("Fullname  "+widget.todo.firstname+" "+widget.todo.lastname, style: TextStyle(fontSize: 16.0, color: Colors.white),),
                                ),
                                Divider(),
                                Padding(
                                padding: const EdgeInsets.only(bottom:1.0),
                                child: Text("PhoneNumber  "+widget.todo.phone, style: TextStyle(fontSize: 16.0, color: Colors.white),),
                                ),
                                Divider(),
                                Padding(
                                padding: const EdgeInsets.only(bottom:1.0),
                                child: Text("Email Address  "+widget.todo.email, style: TextStyle(fontSize: 16.0, color: Colors.white),),
                                ),
                                
                                ],
                                crossAxisAlignment: CrossAxisAlignment.start,
                                ),
                                flex: 9,
                                ),
                                Expanded(
                                child: Icon(FontAwesomeIcons.userCircle,color: Colors.white,),
                                )

                                ],
                                ),
                                )
                                ),
                                  ),
                                 Divider(height: 10,),
                               Padding(
                                padding: const EdgeInsets.all(12.0),
                                child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                Expanded(
                                child: Column(
                                children: <Widget>[
                                Padding(
                                padding: const EdgeInsets.only(bottom:0),
                                child: 
                                FlatButton.icon(
                                color: Colors.blueAccent,
                                onPressed: (){
                                  checkForCoodinates();
                                  
                         
                                
                                },
                                icon: Icon(Icons.location_searching,color: Colors.white,), label: Text("Geolocation",style: TextStyle(color: Colors.white),)
                                ),
                                ),

                                ],
                                crossAxisAlignment: CrossAxisAlignment.start,
                                ),

                                ),

                                Expanded(
                                child: Column(
                                children: <Widget>[
                                Padding(
                                padding: const EdgeInsets.all(0),
                                child: 
                                FlatButton.icon(
                                color: Colors.lightBlueAccent,
                                onPressed: (){
                                viewService();
                                },
                                icon: Icon(Icons.group_work,color: Colors.white,), label: Text("Services offered",style: TextStyle(color: Colors.white),)
                                ),
                                ),
                               
                                ],
                                crossAxisAlignment: CrossAxisAlignment.start,
                                ),

                                ),

                                ],
                                ),
                                ),


                                ], ) ,
                                ),]  
                                ),

                                );
                                }

                                }
                                
                                class Coodinates{
                                  double longtude;
                                  double latitude;
                                  Coodinates(this.longtude,this.latitude);
                                }
                                class ServiceID{
                                  String serviceID;
                                  String message;
                                  ServiceID(this.serviceID,this.message);
                                }
