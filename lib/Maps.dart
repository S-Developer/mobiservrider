import 'dart:async';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:intl/intl.dart';
import 'package:mobiservrider/TaskDetails.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:http/http.dart' as http;
// import 'package:geolocator/geolocator.dart';
class MyLocation extends StatefulWidget {
  Coodinates coodinates;
   MyLocation({Key key, this.coodinates}) : super(key: key);
  @override
  _MyLocationState createState() => _MyLocationState();
}


class _MyLocationState extends State<MyLocation> {
 static  LatLng _center;
  Completer<GoogleMapController> _controller = Completer();
  Set<Marker> markers = Set();
  MapType _currentMapType = MapType.hybrid;
  LatLng centerPosition;
  double _latitude ,_longitude;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    setState(() {
     _center = LatLng(widget.coodinates.longtude,widget.coodinates.latitude); 
     _onAddMarkerButtonPressed();
    });
  }

   void _onMapCreated(GoogleMapController controller) {
    _controller.complete(controller);
  }

  void _onMapTypeButtonPressed() {
    setState(() {
      _currentMapType = _currentMapType == MapType.normal
          ? MapType.satellite
          : MapType.normal;
      print("dddd" + _currentMapType.toString());
    });
  }
  @override
  Widget build(BuildContext context) {
     return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: (){
                Navigator.pop(context);
              },)
          ],
          title: Text(
            'Delivery Location',
            style: TextStyle(color: Colors.white),
          ),

        ),
        body: Stack(
          children: <Widget>[
            GoogleMap(
              onMapCreated: _onMapCreated,
              mapType: _currentMapType,
              myLocationEnabled: true,
              markers: markers,
              onCameraMove: _onCameraMove,
              initialCameraPosition: CameraPosition(
                target: _center,
                zoom: 11.0,
              ),
            ),
             Padding(
              padding: const EdgeInsets.all(10.0),
              child: Align(
                alignment: Alignment.bottomLeft,
                child: new FloatingActionButton(
                  onPressed: _onMapTypeButtonPressed,
                  child: new Icon(
                    Icons.map,
                    color: Colors.white,
                  ),
                ),
              ),
            ),
            //  Padding(
            //   padding: const EdgeInsets.all(0.0),
            //   child: Align(
            //     alignment: Alignment.bottomCenter,
            //     child: new FloatingActionButton(
            //       onPressed: _onMapTypeButtonPressed,
            //       child: new Icon(
            //         Icons.map,
            //         color: Colors.white,
            //       ),
            //     ),
            //   ),
            // ),
            //  Padding(
            //   padding: const EdgeInsets.all(0.0),
            //   child: Align(
            //     alignment: Alignment.bottomRight,
            //     child: new FloatingActionButton(
            //       onPressed: _onMapTypeButtonPressed,
            //       child: new Icon(
            //         Icons.pin_drop,
            //         color: Colors.white,
            //       ),
            //     ),
            //   ),
            // ),
           
                              
                            ],
                          ),
                        ),
                        
                       );
                    }
                  
                    void _onAddMarkerButtonPressed() {
    InfoWindow infoWindow =
    InfoWindow(title: "Location" + markers.length.toString());
    Marker marker = Marker(
      markerId: MarkerId(markers.length.toString()),
      infoWindow: infoWindow,
      position: _center,
      icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueRed),
    );
    setState(() {
      markers.add(marker);
      

    });
  }
  
  void _onCameraMove(CameraPosition position) {
    centerPosition = position.target;
  }
}