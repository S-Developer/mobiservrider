import 'dart:async';

import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:mobiservrider/Chat/Chat.dart';
import 'package:mobiservrider/Utils/profile.dart';



class ChatPage extends StatefulWidget {

  

  @override
  _ChatPageState createState() => _ChatPageState();
}

class _ChatPageState extends State<ChatPage> {


     @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: DefaultTabController(
        length: 3,
        child: Scaffold(
          appBar:AppBar(
            leading: Icon(FontAwesomeIcons.facebookMessenger),
            title: Text("Chat"),
            bottom: TabBar(
              tabs: <Widget>[
                Tab(text: 'My Chats'),
                Tab(text: 'Contact List'),
                Tab(text: "Notifications",)
                
              ],
            ),
          )
          ,
          body: TabBarView(
            children: <Widget>[
              ListAccomodation(),
               AndroidAction2(),
               AndroidAction3(),
            
            ],
          ),),
      ),
     
    );
  }

}
class ListAccomodation extends StatefulWidget{
  @override
  ListAccomodationState createState() {
   
    return ListAccomodationState();
 } }
    
    class ListAccomodationState extends State<ListAccomodation> {
      final formKey = new GlobalKey<FormState>();
      

  initState(){
    super.initState();
    setState(() {
    
    });
  }

  @override
  Widget build(BuildContext context) {
 // TODO: implement build
              return Center();
          
            }
          
    }


  class AndroidAction2 extends StatefulWidget{

  @override
  AndroidActionApp2 createState() {
    // TODO: implement createState
    return AndroidActionApp2();
  }
      
    }
    
    class AndroidActionApp2 extends State<AndroidAction2> {
     final dataRefrence = FirebaseDatabase.instance.reference().child('Profile');

  List<Profile> items;
  StreamSubscription<Event> _onCounsellorAddedSubscription;

  @override
  void initState(){
    super.initState();
    items = new List();
    _onCounsellorAddedSubscription = dataRefrence.onChildAdded.listen(_onNoteAdded);
  }

  void dispose(){
    _onCounsellorAddedSubscription.cancel();
    super.dispose();
  }
  void _onNoteAdded(Event event){
    setState(() {
     items.add(new Profile.fromSnapshot(event.snapshot)); 
    });
  }
 

  @override
  Widget build(BuildContext context) {

 return Scaffold(
     
      body: Container(
        child: FutureBuilder(
          builder: (_,snapshot){
            if(snapshot.connectionState == ConnectionState.waiting){
              return Center(child: Text('Loading.......'),
              );
              
            }
            else{
              return ListView.builder(
        // padding: const EdgeInsets.all(5),
          itemCount: items.length,
          itemBuilder: (context,position){
            return Card(
              elevation: 2,
              child: Column(
              children: <Widget>[
                ListTile(
                  title: Text(
                    '${items[position].fullname}',
                    style: TextStyle(fontSize: 18.0),
                  ),
                  subtitle: Text(
                    '${items[position].email}',
                  ),
                  trailing: Icon(Icons.offline_pin),
                 // dense: true,
                // selected: true,
                 onTap: (){

               var route = new MaterialPageRoute(
                  builder: (BuildContext context) =>
                      new Chat(value: items[position].id,username:items[position].fullname));
                Navigator.of(context).push(route);
                  //  Navigator.push(context,MaterialPageRoute(builder: (context) => Chat(value: items[position].fullname)));
                 },
                  
                  leading: Container(
                    width: 60,
                    height: 60,
                    decoration: new BoxDecoration(
                      shape: BoxShape.circle,
                      image: new DecorationImage(
                        image: new NetworkImage(
                          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTvzqqu92BGX_nYWmj4sH1_NQmMJZhSyUbRBF4yYi9nLuoEtJ2g&s',
                        ),
                        fit: BoxFit.fill,
                        
                      )
                    ),
            
                  ),
                )
              ],
              )
            );
          },
              );
            }
          },
        )
      
      ));
      
  }
  
  }

  


   class AndroidAction3 extends StatefulWidget{

  @override
  AndroidActionApp3 createState() {
    // TODO: implement createState
    return AndroidActionApp3();
  }
      
    }
    
    class AndroidActionApp3 extends State<AndroidAction3> {
        @override
  Widget build(BuildContext context) {
      
    return Center();
  }
         
  }