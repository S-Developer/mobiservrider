
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';

class Chat extends StatefulWidget {
  static const String id = "CHAT";
   final String value;
   final String username;
    Chat({Key key, this.value,this.username}) : super(key: key);
    FirebaseUser user;
  @override
  _ChatState createState() => _ChatState();
}

class _ChatState extends State<Chat> {
   Future _data;
   String _hey_lee;


  final FirebaseAuth _auth = FirebaseAuth.instance;
  final Firestore _firestore = Firestore.instance;

  String _userEmail;
  String _userID;
  String _recipientID;
  String _tokenID;
  String _counsellorID;

  TextEditingController messageController = TextEditingController();
  ScrollController scrollController = ScrollController();

  Future getEmail() async{
     FirebaseUser user = await FirebaseAuth.instance.currentUser();
     String myEmail = user.email;
     String userID= user.uid;
     String tmpToken = widget.value;
     String generatedToken;
     if(tmpToken.compareTo(userID)<0){
       generatedToken = tmpToken +userID;
     }
     else{
        generatedToken = userID+ tmpToken;
     }
   
     setState(() {
      _userEmail = myEmail; 
      _userID = userID;
      _tokenID = generatedToken;
      _counsellorID =tmpToken;
     
     });
  }

  Future startChart() async{
    var dbLink = FirebaseDatabase.instance.reference().child('ActiveChats').child(_tokenID);
    dbLink.once().then((DataSnapshot data){
      if (data.value == null){
        dbLink.set({'ClientID':_userID,'CouncellorID': _counsellorID, 'Status':'Active','Useremail':_userEmail,
            'created_at':new DateTime.now().millisecondsSinceEpoch});
      }
  setState(() {
    });
    
  });


  }

  @override
  void initState() {
    super.initState();
     _data = getEmail();
   //  _data2 = startChart();
     }

  Future<void> callback() async {
    startChart();
    if (messageController.text.length > 0) {
       await _firestore.collection('messages').document(_tokenID).collection('Chats').add({
        'text': messageController.text,
        'from': _userEmail,
        'timestamp': DateTime.now().millisecondsSinceEpoch.toString(),
      });
      messageController.clear();
      scrollController.animateTo(
        scrollController.position.maxScrollExtent,
        curve: Curves.easeOut,
        duration: const Duration(milliseconds: 300),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: Hero(
          tag: 'logo',
          child: Container(
                   
                      child: Icon(Icons.chat_bubble),
                    ),
             
        ),
        title: Text(widget.username),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.close),
            onPressed: () {
            //  _auth.signOut();
            // Add rate and exit option here
              Navigator.of(context).popUntil((route) => route.isFirst);
            },
          )
        ],
      ),
      body: SafeArea(
        child: Column(
          
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Expanded(
              child: StreamBuilder<QuerySnapshot>(
                stream: _firestore.collection('messages').document(_tokenID).collection('Chats').orderBy('timestamp', descending: false).snapshots(),
                builder: (context, snapshot) {
                  if (!snapshot.hasData)
                    return Center(
                      child: CircularProgressIndicator(),
                    );

                  List<DocumentSnapshot> docs = snapshot.data.documents;

                  List<Widget> messages = docs
                      .map((doc) => Message(
                            from: doc.data['from'],
                            text: doc.data['text'],
                            me: _userEmail == doc.data['from'],
                          ))
                      .toList();

                  return ListView(
                    controller: scrollController,
                    children: <Widget>[
                      ...messages,
                    ],
                  );
                },
              ),
            ),
            Container(
               margin: const EdgeInsets.symmetric(horizontal:8.0),
              child: Row(
                children: <Widget>[
                  Expanded(
                    child: TextField(
                      onSubmitted: (value) => callback(),
                      decoration: InputDecoration(
                        hintText: "Start typing...",
                      ),
                      controller: messageController,
                    ),
                  ),
                  new Container(
                    margin: const EdgeInsets.symmetric(horizontal: 4.0),
                    child: new IconButton(
                      icon:  new Icon(Icons.send),
                      onPressed: ()=>callback(),
                    ),
                  ),
                
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class SendButton extends StatelessWidget {
  final String text;
  final VoidCallback callback;

  const SendButton({Key key, this.text, this.callback}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return FlatButton(
      color: Colors.orange,
      onPressed: callback,
      child: Text(text),
    );
  }
}

class Message extends StatelessWidget {
  final String from;
  final String text;

  final bool me;

  const Message({Key key, this.from, this.text, this.me}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: BoxConstraints(maxWidth:2),
      margin: EdgeInsets.all(10.0),
      child: Column(
        
        crossAxisAlignment:
            me ? CrossAxisAlignment.end : CrossAxisAlignment.start,
        children: <Widget>[
          Text(''
           // from,
          ),
          Material(
            
            color: me ? Colors.teal : Colors.grey,
            borderRadius: BorderRadius.circular(10.0),
            elevation: 6.0,
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 15.0),
              child: Text(
                text,
              ),
              
            ),
          )
        ],
      ),
    );
  }
}