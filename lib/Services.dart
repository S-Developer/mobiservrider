                                                import 'dart:convert';
                                                import 'package:http/http.dart' as http;
                                                import 'package:flutter/material.dart';

                                                import 'TaskDetails.dart';
                                              class Services extends StatefulWidget {
                                              ServiceID servi;

                                              Services({Key key, this.servi}) : super(key: key);
                                              @override
                                              _ServicesState createState() => _ServicesState();
                                              }

                                              class _ServicesState extends State<Services> {
                                              List servicedata;
                                              Future<String> getData() async{
                                              var response = await http.get(
                                              Uri.encodeFull('http://nortonconstituencyzim.co.zw/json/getRequestedService.php'),
                                              headers:{
                                              "Accept": "application/json",
                                              }
                                              );
                                              print(response.body);
                                              this.setState((){
                                              servicedata = json.decode(response.body);


                                              });
                                              return "Success!";
                                              }
                                              @override
                                              void initState(){
                                              super.initState();
                                              this.getData();
                                              }
                                              @override
                                              Widget build(BuildContext context) {
                                              return Scaffold(
                                              appBar: AppBar(title: Text("Requested Service"),),
                                              body:GridView.builder(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            itemCount: servicedata ==null?0 :servicedata.length,
           // itemCount: servicedata.length,
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 1,childAspectRatio: 1,),
            itemBuilder: (contxt, indx){
              return Card(
                child: new Column(
                        children: <Widget>[
                          new Hero(
                            tag: servicedata[indx]['servicetitle'],
                            child: new Material(
                              child: new InkWell(
                                onTap: (){

                              
                                    },
                                child: Image.network(
                                  servicedata[indx]['ImgUrils'],
                                    fit: BoxFit.fill,
                                    height: 200.0,
                                    alignment: Alignment.center,
                                    filterQuality: FilterQuality.low,
                                   )
                                
                              ),
                            
                            ),
                          ),
                            Padding(padding: new EdgeInsets.all(2.0)),
                            new Text(servicedata[indx]['servicetitle'],style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),),
                             new Text(servicedata[indx]['details'],style: TextStyle(fontSize: 14),),
                            
                        ],
                      ),
              );
            },
          ),

                                              );
                                              }
                                              }
                                             