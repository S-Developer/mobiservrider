import 'package:flutter/material.dart';
import 'TaskDetails.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
class CustomerCart extends StatefulWidget {
  ServiceID servi;
  CustomerCart({Key key, this.servi}) : super(key: key);
  @override
  _CustomerCartState createState() => _CustomerCartState();
}

class _CustomerCartState extends State<CustomerCart> {
  List tasksdata;
  Future<String> getData() async{
    var response = await http.get(
      Uri.encodeFull('http://nortonconstituencyzim.co.zw/json/getCustomerCart.php'),
      headers:{
        "Accept": "application/json",
      }
    );
    print(response.body);
    this.setState((){
     tasksdata = json.decode(response.body);

     
    });
    return "Success!";
  }
  @override
  void initState(){
    super.initState();
    this.getData();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Customer Cart"),
      ),
      body: Container(
        child: ListView.builder(
        itemCount: tasksdata ==null?0 :tasksdata.length,
        // padding: const EdgeInsets.all(5),
         itemBuilder: (BuildContext context,int index){
            return GestureDetector(

          child: Card(
            child: new Column(
              children: <Widget>[
                new Row(
                 crossAxisAlignment: CrossAxisAlignment.start, //Vertical alignment: starting edge alignment
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    new Expanded(
                     child:new Container(
                    height: 80.0,
                    width: 70.0,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(5),
                        topLeft: Radius.circular(5)
                      ),
                      image: DecorationImage(
                        fit: BoxFit.cover,
                        image: NetworkImage(
                          tasksdata[index]['ImgUrils']
                        )
                      )
                    ),
                  ),
                      flex: 1,
                    ),
                    new Expanded(
                      child: Container(
                        height: 95.0,
                        margin: new EdgeInsets.fromLTRB(5.0, 0.0, 0.0, 0.0),
                        child: new Column(
                          children: <Widget>[
                          
                            new Container(
                              padding: EdgeInsets.fromLTRB(0, 10, 40, 0),
                              child: new Text(
                                tasksdata[index]['servicetitle'],
                                style: new TextStyle(
                                    fontSize: 16.0,
                                    fontWeight: FontWeight.w700),
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                              ),
                              alignment: FractionalOffset.topLeft,
                            ),
                            new Container(
                              child: new Text(tasksdata[index]['details'],
                                  style: new TextStyle(fontSize: 14.0),
                                  maxLines: 2,
                                  overflow: TextOverflow.ellipsis),
                              alignment: Alignment.topLeft,
                            ),
                            new Expanded(
                              child: new Container(
                                margin:
                                    new EdgeInsets.fromLTRB(0.0, 2.0, 0.0, 0.0),
                                child: new Stack(
                                  children: <Widget>[
                                    //  new Container(
                                    //   child: new Text("tasksdata[index]['Price']",
                                    //       style: new TextStyle(fontSize: 16.0,fontWeight:FontWeight.w800 )),
                                    //        alignment: FractionalOffset.topLeft,
                                    
                                    // ),
                                    
                                  ],
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                      flex: 3,
                    ),
                  ],
                ),
                    // split line
              ],
            ),
          ),
          onTap: () {
           
                     
          });
          }
              )
              ),
      
    );
  }
}